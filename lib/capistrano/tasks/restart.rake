# frozen_string_literal: true

namespace :deploy do
  desc 'Restart the application server'
  task :restart do
    on roles(:app) do
      execute '$RAILS_RESTART_CMD'
    end
  end

  after :finished, :restart
end
