# frozen_string_literal: true

namespace :party_website do
  namespace :lint do
    desc 'Lint just Ruby'
    task :ruby, [:autofix] do |_, args|
      cmd = 'bin/bundle exec rubocop'
      cmd += ' --safe-auto-correct' if args[:autofix]
      sh cmd
    end

    desc 'Lint only JS'
    task :js, [:autofix] do |_, args|
      files = [
        'babel.config.js',
        "'config/webpack/*.js'",
        "'frontend/**/*.js'",
        'postcss.config.js'
      ]

      cmd = "bin/yarn standard --env jest #{files.join(' ')}"
      cmd += ' --fix' if args[:autofix]
      sh cmd
    end
  end

  desc 'Run all linters'
  task :lint, [:autofix] => %i[lint:ruby lint:js]
end
