# frozen_string_literal: true

# Tasks in this file are stage-only.
return unless Rails.env.staging?

namespace :party_website do
  namespace :stage do
    namespace :wipe do
      # Wipe database.
      task db: :environment do
        conn = ActiveRecord::Base.connection
        tables = conn.tables - %w[ar_internal_metadata schema_migrations]
        tables.each { |tbl| conn.truncate(tbl) }
      end

      # Wipe attachments storage.
      task :attachments do
        attachments = Dir.glob(Rails.root.join('storage', '*'))
        FileUtils.rm_r(attachments)
      end
    end

    desc 'Wipe all staging app data'
    task wipe: %i[wipe:db wipe:attachments]
  end
end
