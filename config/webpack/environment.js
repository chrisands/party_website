const { environment } = require('@rails/webpacker')

environment.loaders.get('file').test = /\.(jpg|pdf|png|svg)$/i

module.exports = environment
