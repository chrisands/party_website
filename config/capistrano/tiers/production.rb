# frozen_string_literal: true

set :branch, 'deployed'

set :chruby_ruby, 'ruby-2.5.3'

host = ENV.fetch('PROD_DEPLOY_HOST')
server host, user: fetch(:user), roles: %w[app db web]
