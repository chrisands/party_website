# frozen_string_literal: true

set :application, 'party_website'
set :repo_url, "https://gitlab.com/libertarian-party/#{fetch(:application)}.git"

set(:deploy_to, proc { "/var/www/#{fetch(:application)}/#{fetch(:stage)}" })
set(:user, proc { [fetch(:application), fetch(:stage)].join('_') })

set(:linked_files, proc { ["config/credentials/#{fetch(:stage)}.key"] })
set :linked_dirs, %w[log public/assets storage tmp/cache tmp/pids tmp/sockets]

set :keep_assets, 2
set :migration_role, :app

set :ssh_options, forward_agent: false
