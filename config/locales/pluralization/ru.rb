# frozen_string_literal: true

{
  ru: {
    i18n: {
      plural: {
        keys: %i[one few many other],
        rule:
          lambda do |n|
            mod10 = n % 10
            mod100 = n % 100

            if mod10 == 1 && mod100 != 11
              :one
            elsif mod10.in?(2..4) && !mod100.in?(12..14)
              :few
            elsif mod10.zero? || mod10.in?(5..9) || mod100.in?(11..14)
              :many
            else
              :other
            end
          end
      }
    }
  }
}
