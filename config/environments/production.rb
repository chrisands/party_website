# frozen_string_literal: true

# Settings specified here will take precedence over those
# in config/application.rb.
Rails.application.configure do
  # Compress JavaScript with uglifier.
  config.assets.js_compressor = :uglifier

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Enable Action Controller fragment caching.
  config.action_controller.perform_caching = true

  # Set a host that'll be used for URI generation in mailers.
  config.action_mailer.default_url_options = {
    host: ENV.fetch('DOMAIN_NAME')
  }

  # Deliver emails using sendmail.
  config.action_mailer.delivery_method = :sendmail

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # Store uploaded files on the local file system
  # (see config/storage.yml for options).
  config.active_storage.service = :local

  # Send deprecation notifications to registered listeners.
  config.active_support.deprecation = :notify

  # Don't reload application classes and modules on each request.
  config.cache_classes = true

  # Disable full error reports.
  config.consider_all_requests_local = false

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [:request_id]

  # Disable serving static files from the /public folder since Apache
  # or NGINX already handles this.
  config.public_file_server.enabled = false
end
