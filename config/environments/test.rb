# frozen_string_literal: true

# Settings specified here will take precedence over those
# in config/application.rb.
Rails.application.configure do
  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Disable Action Controller fragment caching.
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Set a host that'll be used for URI generation in mailers.
  config.action_mailer.default_url_options = {
    host: 'testhost'
  }

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Raise error for missing translations.
  config.action_view.raise_on_missing_translations = true

  # Store uploaded files on the local file system in a temporary directory
  # (see config/storage.yml for details).
  config.active_storage.service = :test

  # Print deprecation notifications to the stderr.
  config.active_support.deprecation = :stderr

  # Don't reload application classes and modules on each request.
  config.cache_classes = true

  # Don't actually store anything in cache.
  config.cache_store = :null_store

  # Show full error reports.
  config.consider_all_requests_local = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.to_i}"
  }
end
