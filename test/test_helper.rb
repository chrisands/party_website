# frozen_string_literal: true

ENV['DOMAIN_NAME'] = 'testhost'

require File.expand_path('../config/environment', __dir__)
require 'rails/test_help'

require 'webmock/minitest'
WebMock.disable_net_connect!(allow_localhost: true)

module ActiveSupport
  class TestCase
    # Include FactoryBot methods: #build, #create, #attributes_for, etc.
    include FactoryBot::Syntax::Methods

    def after_teardown
      super

      # Remove files uploaded during test.
      config = Rails.configuration.active_storage['service_configurations']
      return unless config

      storage_dir = config['test']['root']
      FileUtils.rm_rf(storage_dir)
    end
  end
end

module ActionDispatch
  class IntegrationTest
    def login(params)
      post admin_sessions_url, params: params
    end

    def assert_forbidden
      assert_redirected_to admin_root_url
      assert_equal I18n.t('forbidden'), flash[:alert]
    end
  end
end
