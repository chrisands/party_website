# frozen_string_literal: true

require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  def setup
    @user = build(
      :user,
      id: '3317e507-628b-47e0-956a-dcd1594cd3c0',
      name: 'Full Name'
    )

    # Redefine password reset token generation for predictable results.
    class << @user
      def generate_reset_password_token!
        self.reset_password_token = '9aboi_V7yUxbU9YtDkNg'
      end
    end
  end

  def test_reset_password_email_delivery
    mail = UserMailer.reset_password(@user)

    assert_emails 1 do
      mail.deliver_now
    end
  end

  def test_reset_password_email_headers
    mail = UserMailer.reset_password(@user)
    mail.deliver_now

    assert_equal ['noreply@testhost'], mail.from
    assert_equal [@user.email], mail.to
    assert_equal I18n.t('user_mailer.reset_password.subject'), mail.subject
  end

  def test_reset_password_email_body
    mail = UserMailer.reset_password(@user)
    mail.deliver_now

    assert_equal read_fixture('reset_password.txt').join, mail.body.to_s
  end
end
