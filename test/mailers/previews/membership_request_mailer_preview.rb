# frozen_string_literal: true

class MembershipRequestMailerPreview < ActionMailer::Preview
  def follow_up
    MembershipRequestMailer.follow_up(MembershipRequest.first)
  end
end
