# frozen_string_literal: true

require 'test_helper'

class MembershipRequestMailerTest < ActionMailer::TestCase
  def setup
    @membership_request = build(
      :membership_request,
      email: 'foobar@email.example',
      full_name: 'Full Name'
    )
  end

  def test_follow_up_email_delivery
    mail = MembershipRequestMailer.follow_up(@membership_request)

    assert_emails 1 do
      mail.deliver_now
    end
  end

  def test_follow_up_email_headers
    mail = MembershipRequestMailer.follow_up(@membership_request)
    mail.deliver_now

    assert_equal ['noreply@testhost'], mail.from
    assert_equal [@membership_request.email], mail.to
    assert_equal I18n.t('membership_request_mailer.follow_up.subject'),
                 mail.subject
  end

  def test_follow_up_email_body
    mail = MembershipRequestMailer.follow_up(@membership_request)
    mail.deliver_now

    assert_equal read_fixture('follow_up.html').join, mail.html_part.body.to_s
    assert_equal read_fixture('follow_up.txt').join, mail.text_part.body.to_s
  end
end
