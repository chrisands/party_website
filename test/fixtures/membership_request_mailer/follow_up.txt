Мы свяжемся с вами в течение 10 дней.
Если в заявке нужно что-то изменить или уточнить, или если у вас возник иной вопрос ― пишите нашему менеджеру по работе с заявками на почту newblood@libertarian-party.ru.

А пока подписывайтесь на информационные ресурсы партии:

- Телеграм https://t.me/lpr_tg
- Вконтакте https://vk.com/lpr_vk
- Твиттер https://twitter.com/lpr_tw
- Ютуб https://www.youtube.com/lpr_yt
- Фейсбук https://www.facebook.com/lpr.fcbk
- Инстаграм https://www.instagram.com/lpr_ig

