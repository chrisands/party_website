# frozen_string_literal: true

FactoryBot.define do
  factory :federal_subject, aliases: [:moscow] do
    name 'Москва'
  end
end
