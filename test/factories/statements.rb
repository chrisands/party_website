# frozen_string_literal: true

FactoryBot.define do
  factory :statement do
    association :author, factory: :user

    trait :published do
      association :thumbnail, factory: :picture
      annotation 'ФК ЛПР о непризнании результатов выборов'
      body JSON.generate(
        blocks: [
          text: '18 марта прошло голосование, официальная явка составила…'
        ],
        entityMap: {}
      )
      published true
      published_at { Date.yesterday }
      title 'Выборы 18-го марта'
    end
  end
end
