# frozen_string_literal: true

FactoryBot.define do
  factory :picture do
    file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('test', 'fixtures', 'files', 'ancap-smiley.jpg'),
        'image/jpg'
      )
    end
  end
end
