# frozen_string_literal: true

require 'test_helper'

class MembershipRequestsControllerTest < ActionDispatch::IntegrationTest
  def test_new
    get new_membership_request_url
    assert_response :success
  end

  def test_create
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_difference('MembershipRequest.count') do
      post membership_requests_url, params: params
    end
    assert_redirected_to success_url
  end

  def test_that_mailer_is_called_if_email_is_present
    params = {
      membership_request: build(:membership_request).attributes
    }
    params[:membership_request][:email] = 'some@email.example'

    assert_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end

    mail = ActionMailer::Base.deliveries.last
    assert_equal [params[:membership_request][:email]], mail.to
  end

  def test_that_mailer_is_not_called_without_email_provided
    params = {
      membership_request: build(:membership_request).attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end
  end

  def test_that_mailer_is_not_called_unless_record_saved
    params = {
      membership_request: MembershipRequest.new.attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post membership_requests_url, params: params
    end
  end
end
