# frozen_string_literal: true

require 'test_helper'

module Admin
  class InvitesControllerTest < ActionDispatch::IntegrationTest
    def setup
      user = create(:user, password: 'password')
      login(email: user.email, password: 'password')
    end

    def test_index
      get admin_invites_url
      assert_forbidden
    end

    def test_show
      invite = create(:invite)
      get admin_invite_url(invite)
      assert_forbidden
    end

    def test_new
      get new_admin_invite_url
      assert_forbidden
    end

    def test_create
      params = {
        invite: attributes_for(:invite, comment: 'some comment')
      }

      assert_no_difference('Invite.count') do
        post admin_invites_url, params: params
      end
      assert_forbidden
    end

    def test_destroy
      invite = create(:invite)
      assert_no_difference('Invite.count') do
        delete admin_invite_url(invite)
      end
      assert_forbidden
    end
  end

  class AdminInvitesControllerTest < ActionDispatch::IntegrationTest
    def setup
      admin_user = create(:user, password: 'password')
      admin_user.admin = true
      admin_user.save(validate: false)

      login(email: admin_user.email, password: 'password')
    end

    def test_index
      get admin_invites_url
      assert_response :success
    end

    def test_show
      invite = create(:invite)
      get admin_invite_url(invite)
      assert_response :success
    end

    def test_new
      get new_admin_invite_url
      assert_response :success
    end

    def test_create
      params = {
        invite: attributes_for(:invite, comment: 'some comment')
      }

      assert_difference('Invite.count') do
        post admin_invites_url, params: params
      end
      assert_redirected_to admin_invite_url(Invite.last)
    end

    def test_destroy
      invite = create(:invite)
      assert_difference('Invite.count', -1) do
        delete admin_invite_url(invite)
      end
      assert_redirected_to admin_invites_url
    end
  end
end
