# frozen_string_literal: true

require 'test_helper'

module Admin
  class PasswordsControllerTest < ActionDispatch::IntegrationTest
    def setup
      @user = create(:user)
      @user.generate_reset_password_token!

      @params = {
        token: @user.reset_password_token,
        password: 'new password',
        password_confirmation: 'new password'
      }
    end

    def test_edit_without_token
      get edit_admin_user_password_url(@user)
      assert_response :bad_request
    end

    def test_edit_with_wrong_token
      token = 'wrong token'
      get edit_admin_user_password_url(@user), params: {token: token}
      assert_response :bad_request
    end

    def test_edit_with_expired_token
      @user.update(reset_password_token_expires_at: Time.now - 1.minute)

      token = @user.reset_password_token
      get edit_admin_user_password_url(@user), params: {token: token}
      assert_response :bad_request
    end

    def test_edit_with_fresh_token
      token = @user.reset_password_token
      get edit_admin_user_password_url(@user), params: {token: token}
      assert_response :success
    end

    def test_update_without_token
      @params.delete(:token)
      patch admin_user_password_url(@user), params: @params
      assert_response :bad_request
    end

    def test_update_with_wrong_token
      @params[:token] = 'wrong token'
      patch admin_user_password_url(@user), params: @params
      assert_response :bad_request
    end

    def test_update_with_expired_token
      @user.update(reset_password_token_expires_at: Time.now - 1.minute)
      patch admin_user_password_url(@user), params: @params
      assert_response :bad_request
    end

    def test_update_with_wrong_password_confirmation
      @params[:password_confirmation] = 'wrong confirmation'
      patch admin_user_password_url(@user), params: @params
      assert_response :success
    end

    def test_update_with_fresh_token_and_right_password_confirmation
      patch admin_user_password_url(@user), params: @params
      assert_redirected_to login_url
    end
  end
end
