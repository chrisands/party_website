# frozen_string_literal: true

class MembershipRequestMailer < ApplicationMailer
  def follow_up(membership_request)
    @membership_request = membership_request
    mail(to: membership_request.email)
  end
end
