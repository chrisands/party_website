# frozen_string_literal: true

class ValidUserFactory
  attr_reader :custom_params, :produced_count

  def initialize(custom_params = {})
    @custom_params = custom_params
    @produced_count = 0
  end

  def produce
    inc_produced_count
    User.new(merge_with_defaults(custom_params))
  end

  private

  def inc_produced_count(step: 1)
    @produced_count += step
  end

  def merge_with_defaults(params)
    merged = default_params.merge(params)

    if params[:encrypted_password] && params[:salt]
      merged.delete(:password)
      merged.delete(:password_confirmation)
    else
      merged[:password_confirmation] = merged[:password]
    end

    merged
  end

  def default_params
    {
      email: "user#{produced_count}@email.example",
      invite: Invite.new,
      name: "user#{produced_count}",
      password: 'password',
      password_confirmation: 'password'
    }
  end
end
