# frozen_string_literal: true

class FederalSubject < ApplicationRecord
  has_one :regional_branch
  has_many :membership_requests
end
