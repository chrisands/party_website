# frozen_string_literal: true

class Illustration < ApplicationRecord
  belongs_to :post
  has_one :picture, as: :picturable

  accepts_nested_attributes_for :picture
end
