# frozen_string_literal: true

class Post < ApplicationRecord
  VALID_TYPES = %w[Article News Statement].freeze

  validates :type, inclusion: {in: VALID_TYPES}

  validate :type_corresponds_to_class

  def self.types
    VALID_TYPES
  end

  private

  def type_corresponds_to_class
    errors.add(:type, :not_equal_to_class) unless type == self.class.to_s
  end
end
