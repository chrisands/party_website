# frozen_string_literal: true

class PagesController < ApplicationController
  def donate
  end

  def libertarianism
  end

  def party
    render component: 'Page', props: {name: 'party'}
  end

  def platform
    render component: 'Page', props: {name: 'platform'}
  end

  def press
    render component: 'Page', props: {name: 'press'}
  end

  def success
  end
end
