# frozen_string_literal: true

module Admin
  class PostsController < BaseController
    before_action :set_post, only: %i[edit update destroy]

    def index
      @posts = Post.reorder(created_at: :desc).page(params[:page]).per(9)
    end

    def new
    end

    def create
      @post = post_type.constantize.new
      @post.author = current_user

      if @post.save
        redirect_to edit_admin_post_url(@post)
      else
        render :new
      end
    end

    def edit
      @post.build_thumbnail unless @post.thumbnail
    end

    def update
      if @post.update(post_params)
        redirect_to admin_posts_url, notice: t('.success')
      else
        render :edit
      end
    end

    def destroy
      @post.destroy
      redirect_to admin_posts_url, notice: t('.success')
    end

    private

    def set_post
      @post = Post.find(params[:id])
    end

    def post_type
      @post_type ||= params[:type] if params[:type].in?(Post.types)
    end

    def post_params
      params
        .require(post_type.downcase)
        .permit(
          :annotation,
          :body,
          :published,
          :published_at,
          :title,
          thumbnail_attributes: %i[file id]
        )
    end
  end
end
