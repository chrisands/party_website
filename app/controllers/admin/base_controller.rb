# frozen_string_literal: true

module Admin
  class BaseController < ActionController::Base
    protect_from_forgery with: :exception

    before_action :require_login
    before_action :put_csrf_token_into_cookie

    def require_admin
      not_admin unless current_user.admin?
    end

    private

    layout 'admin'

    def put_csrf_token_into_cookie
      cookies['CSRF-TOKEN'] = masked_authenticity_token(session)
    end

    def not_admin
      redirect_back fallback_location: admin_root_url, alert: t('forbidden')
    end

    # When changing this method's name, don't forget to change Sorcery config.
    def not_logged_in
      redirect_to login_url, alert: t('please_login')
    end
  end
end
