# frozen_string_literal: true

class PostsController < ApplicationController
  def index
    @posts = Post.where(published: true)
                 .reorder(published_at: :desc)
                 .page(params[:page])
                 .per(9)
  end

  def show
    @post = Post.where(published: true).find(params[:id])
  end
end
