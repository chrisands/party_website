/* globals Util */

document.addEventListener("turbolinks:load", function () {
  var selectedItem = function (select) {
    return select.options[select.selectedIndex].text;
  };

  var simulateChangeOn = function (el) {
    var change = new Event("change", {
      "bubbles": true
    });

    el.dispatchEvent(change);
  };

  var form = document.querySelector(".form");
  var federalSubject, residence, district,
      districtField, selectedFederalSubject;

  if (form !== null) {
    federalSubject = document.getElementById("federal-subject-input");
    residence = document.getElementById("residence-input");
    district = document.getElementById("district-input");

    districtField = Util.parentWithClass("form-field", district);
    selectedFederalSubject = selectedItem(federalSubject);

    if (selectedFederalSubject !== "Москва") {
      districtField.classList.add("is-hidden");
    }

    document.addEventListener("change", function (event) {
      if (event.target === federalSubject) {
        selectedFederalSubject = selectedItem(federalSubject);

        if (selectedFederalSubject === "Москва") {
          residence.value = "Москва";
          simulateChangeOn(residence);

          districtField.classList.remove("is-hidden");
        } else {
          residence.value = "";
          district.value = "";

          districtField.classList.add("is-hidden");
        }
      }
    });
  }
});
