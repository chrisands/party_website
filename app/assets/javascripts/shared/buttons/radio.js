document.addEventListener("turbolinks:load", function () {
  var radioButtons = Util.selectAll("[data-radiogroup]");

  if (radioButtons.length > 0) {
    document.addEventListener("click", function (event) {
      if (radioButtons.includes(event.target) && Util.isPrimaryButton(event)) {
        radioButtons.forEach(function (button) {
          if (button.getAttribute("data-radiogroup") ===
            event.target.getAttribute("data-radiogroup")) {
            Util.deactivate(button);
          }
        });
        Util.activate(event.target);
      }
    });
  }
});
