/*
 *= require rails-ujs
 *= require turbolinks
 *
 *= require ./shared/util
 *= require_tree ./shared/buttons
 *
 *= require ./admin/navbar_burger
 *= require ./admin/thumb
 */
