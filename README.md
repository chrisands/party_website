# Сайт Либертарианской партии

По вопросам, связанным с сайтом партии, пишите в телеграм
[@akater](https://t.me/akater) или [@oleynikov](https://t.me/oleynikov).

## Сообщить о баге
[Начните обсуждение][new_issue]. Опишите шаги, которые привели к проблеме.
Прикрепите скриншоты. Чем больше вы предоставите информации, тем быстрее мы
всё починим. Потребуется зарегистрироваться либо войти через Google, Twitter
или GitHub.

## Поучаствовать в разработке
Для начала убедитесь, что у вас установлены:
- Git,
- Ruby,
- Bundler,
- Yarn,
- ImageMagick,
- PostgreSQL.

Клонируйте репозиторий. Скопируйте файл config/examples/dotenv в корневую
директорию проекта с именем .env, и укажите в нём настройки для подключения
к своей базе данных и т. д.
```
$ git clone git@gitlab.com:libertarian-party/party_website.git
$ cd party_website/
$ cp config/examples/dotenv .env
```

Затем выполните следующие команды:
```
$ bin/bundle install --without staging production
$ bin/yarn install
$ bin/rails db:setup
$ RAILS_ENV=test bin/rails db:setup
```

Всё готово к работе!

[new_issue]: https://gitlab.com/libertarian-party/party_website/issues/new
