import React from 'react'
import { shallow } from 'enzyme'

import { QuoteText } from './Quote-Text'

describe('Quote-Text', () => {
  const quoteText = shallow(<QuoteText />)

  it('is <blockquote>', () => {
    expect(quoteText.is('blockquote')).toBe(true)
  })

  it('has class Quote-Text', () => {
    expect(quoteText.prop('className')).toBe('Quote-Text')
  })

  it('contains nothing by default', () => {
    expect(quoteText.children().length).toBe(0)
  })
})

describe('Quote-Text with children', () => {
  const quoteText = shallow(
    <QuoteText>
      There are only two hard things…
    </QuoteText>
  )

  it('contains children', () => {
    expect(quoteText.children().length).toBe(1)
    expect(quoteText.text()).toBe('There are only two hard things…')
  })
})
