import React from 'react'
import { shallow } from 'enzyme'

import { ContactsLabel } from './Contacts-Label'

describe('Contacts-Label', () => {
  const contactsLabel = shallow(<ContactsLabel />)

  it('is <div>', () => {
    expect(contactsLabel.is('div')).toBe(true)
  })

  it('has class Contacts-Label', () => {
    expect(contactsLabel.prop('className')).toBe('Contacts-Label')
  })

  it('contains no children by default', () => {
    expect(contactsLabel.children().length).toBe(0)
  })
})

describe('Contacts-Label with children', () => {
  const contactsLabel = shallow(<ContactsLabel><p>text</p></ContactsLabel>)

  it('now contains children', () => {
    expect(contactsLabel.children().length).toBe(1)
    expect(contactsLabel.contains(<p>text</p>)).toBe(true)
  })
})
