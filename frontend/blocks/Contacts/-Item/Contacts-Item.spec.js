import React from 'react'
import { shallow } from 'enzyme'

import { ContactsItem } from './Contacts-Item'

describe('Contacts-Item', () => {
  const contactsItem = shallow(<ContactsItem />)

  it('is <div>', () => {
    expect(contactsItem.is('div')).toBe(true)
  })

  it('has class Contacts-Item by default', () => {
    expect(contactsItem.prop('className')).toBe('Contacts-Item')
  })

  it('by default does not contain children', () => {
    expect(contactsItem.children().length).toBe(0)
  })
})

describe('Contacts-Item with another class', () => {
  const contactsItem = shallow(<ContactsItem className='Abc' />)

  it('now has one more class', () => {
    expect(contactsItem.prop('className')).toBe('Contacts-Item Abc')
  })
})

describe('Contacts-Item with children', () => {
  const contactsItem = shallow(<ContactsItem><div /></ContactsItem>)

  it('now does contain children', () => {
    expect(contactsItem.children().length).toBe(1)
    expect(contactsItem.contains(<div />)).toBe(true)
  })
})
