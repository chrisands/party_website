import React from 'react'
import { shallow } from 'enzyme'

import { ContactsDesc } from './Contacts-Desc'

describe('Contacts-Desc', () => {
  const contactsDesc = shallow(<ContactsDesc />)

  it('is <div>', () => {
    expect(contactsDesc.is('div')).toBe(true)
  })

  it('has class Contacts-Desc', () => {
    expect(contactsDesc.prop('className')).toBe('Contacts-Desc')
  })

  it('is by default empty', () => {
    expect(contactsDesc.children().length).toBe(0)
  })
})

describe('Contacts-Desc with children', () => {
  const contactsDesc = shallow(<ContactsDesc>description</ContactsDesc>)

  it('it now has children', () => {
    expect(contactsDesc.children().length).toBe(1)
    expect(contactsDesc.render().text()).toBe('description')
  })
})
