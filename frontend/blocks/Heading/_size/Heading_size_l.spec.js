import React from 'react'
import { shallow } from 'enzyme'

import { Heading as BaseHeading } from '../Heading'
import { HeadingSizeL } from './Heading_size_l'

const Heading = HeadingSizeL(BaseHeading)

describe('Heading_size_l', () => {
  const headingSizeL = shallow(<Heading size='l' />)

  it('has classes Heading and Heading_size_l', () => {
    expect(headingSizeL.prop('className')).toBe('Heading Heading_size_l')
  })
})
