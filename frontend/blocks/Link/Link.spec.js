import React from 'react'
import { shallow } from 'enzyme'

import { Link } from './Link'

describe('Link', () => {
  const link = shallow(<Link />)

  it('is <a>', () => {
    expect(link.is('a')).toBe(true)
  })

  it('has class Link by default', () => {
    expect(link.prop('className')).toBe('Link')
  })

  it('contains no predefined href', () => {
    expect(link.prop('href')).toBe(undefined)
  })

  it('is blank by default', () => {
    expect(link.children().length).toBe(0)
  })
})

describe('Link with additional class', () => {
  const link = shallow(<Link className='Hello' />)

  it('now has two classes', () => {
    expect(link.prop('className')).toBe('Link Hello')
  })
})

describe('Link with href', () => {
  const link = shallow(<Link href='https://hi.example' />)

  it('contains that href', () => {
    expect(link.prop('href')).toBe('https://hi.example')
  })
})

describe('Link with some arbitrary attributes', () => {
  const link = shallow(<Link download='foo.pdf' dataBar='baz' />)

  it('includes them', () => {
    expect(link.prop('download')).toBe('foo.pdf')
    expect(link.prop('dataBar')).toBe('baz')
  })
})

describe('Link with children', () => {
  const link = shallow(<Link><h1>hey</h1></Link>)

  it('is not blank', () => {
    expect(link.children().length).toBe(1)
    expect(link.contains(<h1>hey</h1>)).toBe(true)
  })
})
