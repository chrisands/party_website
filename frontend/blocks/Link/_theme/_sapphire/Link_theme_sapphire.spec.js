import React from 'react'
import { shallow } from 'enzyme'

import { Link as BaseLink } from '../../Link'
import { LinkThemeSapphire } from './Link_theme_sapphire'

const Link = LinkThemeSapphire(BaseLink)

describe('Link_theme_sapphire', () => {
  const linkThemeSapphire = shallow(<Link theme='sapphire' />)

  it('has classes Link and Link_theme_sapphire', () => {
    expect(linkThemeSapphire.prop('className')).toBe('Link Link_theme_sapphire')
  })
})
