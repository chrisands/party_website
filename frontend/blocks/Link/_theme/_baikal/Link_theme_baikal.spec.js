import React from 'react'
import { shallow } from 'enzyme'

import { Link as BaseLink } from '../../Link'
import { LinkThemeBaikal } from './Link_theme_baikal'

const Link = LinkThemeBaikal(BaseLink)

describe('Link_theme_baikal', () => {
  const linkThemeBaikal = shallow(<Link theme='baikal' />)

  it('has classes Link and Link_theme_baikal', () => {
    expect(linkThemeBaikal.prop('className')).toBe('Link Link_theme_baikal')
  })
})
