import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonThemePrimary } from './Button_theme_primary'

const Button = ButtonThemePrimary(BaseButton)

describe('Button_theme_primary', () => {
  const buttonThemePrimary = shallow(<Button theme='primary' />)

  it('has classes Button and Button_theme_primary', () => {
    expect(buttonThemePrimary.prop('className'))
      .toBe('Button Button_theme_primary')
  })
})
