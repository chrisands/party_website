import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonThemeDefault } from './Button_theme_default'

const Button = ButtonThemeDefault(BaseButton)

describe('Button_theme_default', () => {
  const buttonThemeDefault = shallow(<Button theme='default' />)

  it('has classes Button and Button_theme_default', () => {
    expect(buttonThemeDefault.prop('className'))
      .toBe('Button Button_theme_default')
  })
})
