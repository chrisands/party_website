import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonThemeEmerald } from './Button_theme_emerald'

const Button = ButtonThemeEmerald(BaseButton)

describe('Button_theme_emerald', () => {
  const buttonThemeEmerald = shallow(<Button theme='emerald' />)

  it('has classes Button and Button_theme_emerald', () => {
    expect(buttonThemeEmerald.prop('className'))
      .toBe('Button Button_theme_emerald')
  })
})
