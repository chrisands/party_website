import React from 'react'
import { shallow } from 'enzyme'

import { SymbolsLogo } from './Symbols-Logo'

describe('Symbols-Logo', () => {
  const symbolsLogo = shallow(<SymbolsLogo />)

  it('is <img>', () => {
    expect(symbolsLogo.is('img')).toBe(true)
  })

  it('has class Symbols-Logo', () => {
    expect(symbolsLogo.prop('className')).toBe('Symbols-Logo')
  })

  it('contains some src', () => {
    expect(symbolsLogo.prop('src')).not.toBe(undefined)
  })
})
