import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'
import Link from '../../Link'

import kuznetsova from '../images/kuznetsova.jpg'
import kuznetsova2x from '../images/kuznetsova@2x.jpg'
import maximov from '../images/maximov.jpg'
import maximov2x from '../images/maximov@2x.jpg'

export const HomeControlCommission = () => (
  <Contacts
    desc={
      <React.Fragment>
        контролирует соблюдение устава, исполнение решений руководящих
        органов партии, а&nbsp;также контролирует финансовую, хозяйственную
        деятельность партии и&nbsp;её региональных отделений. {}
        <Link href='mailto:ckrk@libertarian-party.ru' theme='default'>
          ckrk@libertarian-party.ru
        </Link>
      </React.Fragment>
    }
    title='Центральная контрольно-ревизионная комиссия'
    withLabel>
    <Contact
      location='Москва'
      name='Дмитрий Максимов'
      photo={maximov}
      photo2x={maximov2x}
      position='Председатель центральной контрольно-ревизионной комиссии'
      telegram='Maximov' />
    <Contact
      location='Москва'
      name='Анна Кузнецова'
      photo={kuznetsova}
      photo2x={kuznetsova2x}
      position='Центральная контрольно-ревизионная комиссия' />
  </Contacts>
)
