import { withBemMod } from '@bem-react/core'

import { cnHome } from '../../Home'
import './Home-Tab_active.css'

export const HomeTabActive = withBemMod(cnHome('Tab'), { active: true })
