import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'
import Link from '../../Link'

import kichanova from '../images/kichanova.jpg'
import kichanova2x from '../images/kichanova@2x.jpg'
import tyulenin from '../images/tyulenin.jpg'
import tyulenin2x from '../images/tyulenin@2x.jpg'

export const HomeArbitrationCommittee = () => (
  <Contacts
    desc={
      <React.Fragment>
        орган для апелляции при исключении из&nbsp;партии, отказе
        во&nbsp;вступлении, а&nbsp;также необоснованных решений Федерального
        комитета партии, включая председателя. {}
        <Link href='mailto:arbitration@libertarian-party.ru' theme='default'>
          arbitration@libertarian-party.ru
        </Link>
      </React.Fragment>
    }
    title='Этический комитет'
    withLabel>
    <Contact
      location='Лондон'
      name='Вера Кичанова'
      photo={kichanova}
      photo2x={kichanova2x}
      position='Этический комитет' />
    <Contact
      location='Торонто'
      name='Максим Тюленин'
      photo={tyulenin}
      photo2x={tyulenin2x}
      position='Этический комитет' />
  </Contacts>
)
