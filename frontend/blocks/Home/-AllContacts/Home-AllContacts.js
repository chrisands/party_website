import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'

import akater from '../images/akater.jpg'
import akater2x from '../images/akater@2x.jpg'
import boiko from '../images/boiko.jpg'
import boiko2x from '../images/boiko@2x.jpg'
import kalyonov from '../images/kalyonov.jpg'
import kalyonov2x from '../images/kalyonov@2x.jpg'
import kichanova from '../images/kichanova.jpg'
import kichanova2x from '../images/kichanova@2x.jpg'
import kuznetsova from '../images/kuznetsova.jpg'
import kuznetsova2x from '../images/kuznetsova@2x.jpg'
import maximov from '../images/maximov.jpg'
import maximov2x from '../images/maximov@2x.jpg'
import nozdrin from '../images/nozdrin.jpg'
import nozdrin2x from '../images/nozdrin@2x.jpg'
import osenin from '../images/osenin.jpg'
import osenin2x from '../images/osenin@2x.jpg'
import ovsienko from '../images/ovsienko.jpg'
import ovsienko2x from '../images/ovsienko@2x.jpg'
import prohorov from '../images/prohorov.jpg'
import prohorov2x from '../images/prohorov@2x.jpg'
import samodurov from '../images/samodurov.jpg'
import samodurov2x from '../images/samodurov@2x.jpg'
import svetov from '../images/svetov.jpg'
import svetov2x from '../images/svetov@2x.jpg'
import tyulenin from '../images/tyulenin.jpg'
import tyulenin2x from '../images/tyulenin@2x.jpg'

export const HomeAllContacts = () => (
  <Contacts>
    <Contact
      email='s.boiko@libertarian-party.ru'
      location='Москва'
      name='Сергей Бойко'
      phone='+7 916 127-43-41'
      photo={boiko}
      photo2x={boiko2x}
      position='Председатель'
      telegram='dsboiko' />
    <Contact
      email='samodurov@libertarian-party.ru'
      location='Москва'
      name='Кирилл Самодуров'
      phone='+7 926 416-02-89'
      photo={samodurov}
      photo2x={samodurov2x}
      position='Заместитель председателя'
      telegram='kir_sam' />
    <Contact
      email='secretary@libertarian-party.ru'
      location='Московская область'
      name='Алексей Овсиенко'
      photo={ovsienko}
      photo2x={ovsienko2x}
      position='Ответственный секретарь'
      telegram='Ergilion' />
    <Contact
      email='akater@libertarian-party.ru'
      location='Москва'
      name='Дмитрий Нескоромный'
      photo={akater}
      photo2x={akater2x}
      position='Федеральный комитет' />
    <Contact
      email='kalyonov@libertarian-party.ru'
      location='Москва'
      name='Евгений Калёнов'
      photo={kalyonov}
      photo2x={kalyonov2x}
      position='Федеральный комитет'
      telegram='kalyonov' />
    <Contact
      location='Москва'
      name='Михаил Светов'
      photo={svetov}
      photo2x={svetov2x}
      position='Федеральный комитет'
      telegram='mr_libertarian' />
    <Contact
      location='Москва'
      name='Юрий Ноздрин'
      photo={nozdrin}
      photo2x={nozdrin2x}
      position='Федеральный комитет' />
    <Contact
      email='osenin@libertarian-party.ru'
      location='Москва'
      name='Владимир Осенин'
      photo={osenin}
      photo2x={osenin2x}
      position='Федеральный комитет'
      telegram='osenin' />
    <Contact
      location='Санкт-Петербург'
      name='Николай Прохоров'
      photo={prohorov}
      photo2x={prohorov2x}
      position='Федеральный комитет' />
    <Contact
      location='Москва'
      name='Дмитрий Максимов'
      photo={maximov}
      photo2x={maximov2x}
      position='Председатель центральной контрольно-ревизионной комиссии'
      telegram='Maximov' />
    <Contact
      location='Москва'
      name='Анна Кузнецова'
      photo={kuznetsova}
      photo2x={kuznetsova2x}
      position='Центральная контрольно-ревизионная комиссия' />
    <Contact
      location='Лондон'
      name='Вера Кичанова'
      photo={kichanova}
      photo2x={kichanova2x}
      position='Этический комитет' />
    <Contact
      location='Торонто'
      name='Максим Тюленин'
      photo={tyulenin}
      photo2x={tyulenin2x}
      position='Этический комитет' />
  </Contacts>
)
