import React from 'react'
import { shallow } from 'enzyme'

import { Logo } from './Logo'
import { LogoIcon } from './-Icon/Logo-Icon'
import { LogoText } from './-Text/Logo-Text'

describe('Logo', () => {
  const logo = shallow(<Logo />)

  it('is <div>', () => {
    expect(logo.is('div')).toBe(true)
  })

  it('has class Logo by default', () => {
    expect(logo.prop('className')).toBe('Logo')
  })

  it('contains Logo-Icon', () => {
    expect(logo.contains(<LogoIcon />)).toBe(true)
  })

  it('does not contain Logo-Text by default', () => {
    expect(logo.find(LogoText).length).toBe(0)
  })
})

describe('Logo with extra classes', () => {
  const logo = shallow(<Logo className='Abc Xyz' />)

  it('appends that classes to the default class', () => {
    expect(logo.prop('className')).toBe('Logo Abc Xyz')
  })
})

describe('Logo with iconTheme', () => {
  const logo = shallow(<Logo iconTheme='some-theme' />)

  it('passes that as theme to Logo-Icon', () => {
    expect(logo.contains(<LogoIcon theme='some-theme' />)).toBe(true)
  })
})

describe('Logo with children', () => {
  const logo = shallow(<Logo>with children</Logo>)

  it('contains that children wrapped into Logo-Text', () => {
    const logoText = <LogoText>with children</LogoText>
    expect(logo.contains(logoText)).toBe(true)
  })
})
