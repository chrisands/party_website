import React from 'react'
import { shallow } from 'enzyme'

import Heading from '../../Heading'
import { FooterHeading } from './Footer-Heading'

describe('Footer-Heading', () => {
  const footerHeading = shallow(<FooterHeading />)

  it('is Heading', () => {
    expect(footerHeading.is(Heading)).toBe(true)
  })

  it('has class Footer-Heading', () => {
    expect(footerHeading.prop('className')).toBe('Footer-Heading')
  })

  it('has size="s"', () => {
    expect(footerHeading.prop('size')).toBe('s')
  })

  it('contains no children by default', () => {
    expect(footerHeading.children().length).toBe(0)
  })
})

describe('Footer-Heading with children', () => {
  const footerHeading = shallow(
    <FooterHeading>can have children</FooterHeading>
  )

  it('passes them as children to the underlying Heading', () => {
    const heading = <Heading>can have children</Heading>
    expect(footerHeading.matchesElement(heading)).toBe(true)
  })
})
