import { withBemMod } from '@bem-react/core'

import { cnFooter } from '../../Footer'
import './Footer-ChatLink_desktop.css'

export const FooterChatLinkDesktop = withBemMod(
  cnFooter('ChatLink'),
  { desktop: true }
)
