import React from 'react'
import { shallow } from 'enzyme'

import { BaseFooterChatLink } from '../Footer-ChatLink'
import { FooterChatLinkDesktop } from './Footer-ChatLink_desktop'

const FooterChatLink = FooterChatLinkDesktop(BaseFooterChatLink)

describe('Footer-ChatLink_desktop', () => {
  const footerChatLink = shallow(<FooterChatLink desktop />)

  it('has classes Footer-ChatLink and Footer-ChatLink_desktop', () => {
    expect(footerChatLink.prop('className'))
      .toBe('Footer-ChatLink Footer-ChatLink_desktop')
  })
})
