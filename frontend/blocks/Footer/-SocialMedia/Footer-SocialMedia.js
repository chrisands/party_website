import React from 'react'

import Social from '../../Social'
import { cnFooter } from '../Footer'
import './Footer-SocialMedia.css'

export const FooterSocialMedia = () => (
  <ul className={cnFooter('SocialMedia')}>
    <li>
      <Social
        medium='telegram'
        size='m'
        uri='tg://resolve?domain=lpr_tg' />
    </li>
    <li>
      <Social
        medium='vk'
        size='m'
        uri='https://vk.com/lpr_vk' />
    </li>
    <li>
      <Social
        medium='youtube'
        size='m'
        uri='https://www.youtube.com/lpr_yt' />
    </li>
    <li>
      <Social
        medium='twitter'
        size='m'
        uri='https://twitter.com/lpr_tw' />
    </li>
    <li>
      <Social
        medium='instagram'
        size='m'
        uri='https://www.instagram.com/lpr_ig' />
    </li>
  </ul>
)
