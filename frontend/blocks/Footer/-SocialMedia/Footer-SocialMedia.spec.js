import React from 'react'
import { shallow } from 'enzyme'

import Social from '../../Social'
import { FooterSocialMedia } from './Footer-SocialMedia'

describe('Footer-SocialMedia', () => {
  const footerSocialMedia = shallow(<FooterSocialMedia />)

  it('is <ul>', () => {
    expect(footerSocialMedia.is('ul')).toBe(true)
  })

  it('has class Footer-SocialMedia', () => {
    expect(footerSocialMedia.prop('className')).toBe('Footer-SocialMedia')
  })

  it('includes Social with link to telegram', () => {
    const socialTelegram = (
      <Social medium='telegram' uri='tg://resolve?domain=lpr_tg' />
    )
    expect(footerSocialMedia.containsMatchingElement(socialTelegram)).toBe(true)
  })

  it('has a link to vk', () => {
    const socialVk = <Social medium='vk' uri='https://vk.com/lpr_vk' />
    expect(footerSocialMedia.containsMatchingElement(socialVk)).toBe(true)
  })

  it('contains youtube link as Social', () => {
    const socialYoutube = (
      <Social medium='youtube' uri='https://www.youtube.com/lpr_yt' />
    )
    expect(footerSocialMedia.containsMatchingElement(socialYoutube)).toBe(true)
  })

  it('contains Social with URI of twitter', () => {
    const socialTwitter = (
      <Social medium='twitter' uri='https://twitter.com/lpr_tw' />
    )
    expect(footerSocialMedia.containsMatchingElement(socialTwitter)).toBe(true)
  })

  it('links to instagram', () => {
    const socialInstagram = (
      <Social medium='instagram' uri='https://www.instagram.com/lpr_ig' />
    )
    expect(footerSocialMedia.containsMatchingElement(socialInstagram))
      .toBe(true)
  })
})
