import React from 'react'
import { shallow } from 'enzyme'

import { PageText } from './Page-Text'

describe('Page-Text', () => {
  const pageText = shallow(<PageText />)

  it('is <div>', () => {
    expect(pageText.is('div')).toBe(true)
  })

  it('has class Page-Text', () => {
    expect(pageText.prop('className')).toBe('Page-Text')
  })

  it('is empty by default', () => {
    expect(pageText.children().length).toBe(0)
  })
})

describe('Page-Text with children', () => {
  const pageText = shallow(<PageText><p>some text</p></PageText>)

  it('is not empty anymore', () => {
    expect(pageText.children().length).toBe(1)
    expect(pageText.contains(<p>some text</p>)).toBe(true)
  })
})
