import React from 'react'
import { shallow } from 'enzyme'

import Footer from '../../Footer'
import { PageFooter } from './Page-Footer'

describe('Page-Footer', () => {
  const pageFooter = shallow(<PageFooter />)

  it('is Footer', () => {
    expect(pageFooter.is(Footer)).toBe(true)
  })

  it('has class Page-Footer', () => {
    expect(pageFooter.prop('className')).toBe('Page-Footer')
  })
})
