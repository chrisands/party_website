import React from 'react'
import { withBemMod } from '@bem-react/core'

import Contact from '../../../Contact'
import Contacts from '../../../Contacts'
import Heading from '../../../Heading'
import Paragraph from '../../../Paragraph'
import Symbols from '../../../Symbols'

import { PageContent } from '../../-Content/Page-Content'
import { PageFooter } from '../../-Footer/Page-Footer'
import { PageHeader } from '../../-Header/Page-Header'
import { cnPage } from '../../Page'

import boiko from './photos/boiko.jpg'
import boiko2x from './photos/boiko@2x.jpg'
import petrushenko from './photos/petrushenko.jpg'
import petrushenko2x from './photos/petrushenko@2x.jpg'
import svetov from './photos/svetov.jpg'
import svetov2x from './photos/svetov@2x.jpg'
import './Page_name_press.css'

const newBody = (Base, props) => (
  <Base {...props} >
    <PageHeader theme='default' activeLink='party' />
    <PageContent tag='main'>
      <Heading className={cnPage('Head')} size='l' tag='h1'>
        Для прессы
      </Heading>

      <Heading className={cnPage('Sub')} size='s' tag='h2'>
        Пресс-секретарь и спикеры
      </Heading>
      <Contacts className={cnPage('Contacts')}>
        <Contact
          email='press@libertarian-party.ru'
          name='Дарья Петрушенко'
          phone='+7 977 404-21-99'
          photo={petrushenko}
          photo2x={petrushenko2x}
          position='Пресс-секретарь'
          telegram='DariFY13' />
        <Contact
          email='s.boiko@libertarian-party.ru'
          name='Сергей Бойко'
          phone='+7 916 127-43-41'
          photo={boiko}
          photo2x={boiko2x}
          position='Председатель'
          telegram='dsboiko' />
        <Contact
          email='svetov@libertarian-party.ru'
          name='Михаил Светов'
          photo={svetov}
          photo2x={svetov2x}
          position='Член Федерального комитета партии, политолог,
            автор Ютуб&#8209;канала СВТВ'
          telegram='mr_libertarian' />
      </Contacts>

      <div className={cnPage('Dyad')}>
        <div className={cnPage('AboutParty')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Коротко о партии
          </Heading>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианская партия России (ЛПР) возникла в&nbsp;2008 году
            и&nbsp;сегодня объединяет 850 членов и&nbsp;250 сторонников
            из&nbsp;72 регионов России. Мы&nbsp;появились снизу вверх: никогда
            не&nbsp;было лидера, вокруг которого организовывались&nbsp;бы
            люди, партию создала группа студентов.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианство как политическая философия основывается
            на&nbsp;принципах неагрессии и&nbsp;самопринадлежности. Политическая
            и&nbsp;экономическая программа партии определяются исходя
            из&nbsp;них.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Члены партии были избраны в&nbsp;муниципальные депутаты (2012,
            2014, 2017) в&nbsp;Москве и&nbsp;Подмосковье. Мы&nbsp;регулярно
            участвуем в&nbsp;муниципальных выборах, а&nbsp;также участвовали
            в&nbsp;выборах в&nbsp;Московскую Городскую Думу,
            в&nbsp;Государственную Думу, в&nbsp;региональных выборах мэра
            и&nbsp;в&nbsp;Законодательное Собрание.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Регулярно проводим лекции, дебаты, участвуем в&nbsp;Форумах Свободных
            Людей, Чтениях Адама Смита, Мемориальных Конференциях Айн Рэнд.
            Организовали митинги против Роскомнадзора 30&nbsp;апреля 2018
            и&nbsp;против налогового грабежа 29&nbsp;июля 2018 в&nbsp;Москве.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Мы&nbsp;не&nbsp;сотрудничаем с&nbsp;системной оппозицией.
            Государство отказывается выдавать нашей партии регистрацию,
            мы&nbsp;пытались четыре раза, последний в&nbsp;2015&nbsp;году.
            Наша цель&nbsp;— изменение существующей модели государственного
            устройства России мирными средствами в&nbsp;соответствии
            с&nbsp;либертарианскими ценностями.
          </Paragraph>
        </div>

        <div className={cnPage('Other')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Символика
          </Heading>
          <Symbols />
        </div>
      </div>
    </PageContent>
    <PageFooter isPressButtonDisabled />
  </Base>
)

export const PageNamePress = withBemMod(
  cnPage(),
  { name: 'press' },
  newBody
)
