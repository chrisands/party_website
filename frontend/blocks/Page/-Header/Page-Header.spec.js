import React from 'react'
import { shallow } from 'enzyme'

import Header from '../../Header'
import { PageHeader } from './Page-Header'

describe('Page-Header', () => {
  const pageHeader = shallow(<PageHeader />)

  it('is Header', () => {
    expect(pageHeader.is(Header)).toBe(true)
  })

  it('has class Page-Header', () => {
    expect(pageHeader.prop('className')).toBe('Page-Header')
  })
})
