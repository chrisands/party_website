import React from 'react'
import { shallow } from 'enzyme'

import { ContactBody } from './Contact-Body'

describe('Contact-Body', () => {
  const contactBody = shallow(<ContactBody />)

  it('is <div>', () => {
    expect(contactBody.is('div')).toBe(true)
  })

  it('has class Contact-Body', () => {
    expect(contactBody.prop('className')).toBe('Contact-Body')
  })

  it('does not have children', () => {
    expect(contactBody.children().length).toBe(0)
  })
})

describe('Contact-Body with children', () => {
  const contactBody = shallow(<ContactBody>children here</ContactBody>)

  it('has children now', () => {
    expect(contactBody.children().length).toBe(1)
    expect(contactBody.text()).toBe('children here')
  })
})
