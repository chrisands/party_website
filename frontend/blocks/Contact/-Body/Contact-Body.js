import React from 'react'

import { cnContact } from '../Contact'
import './Contact-Body.css'

export const ContactBody = (props) => (
  <div className={cnContact('Body')}>
    {props.children}
  </div>
)
