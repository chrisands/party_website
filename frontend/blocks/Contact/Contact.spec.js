import React from 'react'
import { shallow } from 'enzyme'

import { Contact } from './Contact'
import { ContactAbout } from './-About/Contact-About'
import { ContactDetails } from './-Details/Contact-Details'
import { ContactEmail } from './-Email/Contact-Email'
import { ContactName } from './-Name/Contact-Name'
import { ContactPhone } from './-Phone/Contact-Phone'
import { ContactPhoto } from './-Photo/Contact-Photo'
import { ContactTelegram } from './-Telegram/Contact-Telegram'

describe('Contact', () => {
  const contact = shallow(<Contact />)

  it('is <div>', () => {
    expect(contact.is('div')).toBe(true)
  })

  it('has class Contact by default', () => {
    expect(contact.prop('className')).toBe('Contact')
  })

  it('contains empty Contact-About by default', () => {
    expect(contact.contains(<ContactAbout />)).toBe(true)
  })

  it('by default contains blank Contact-Details', () => {
    expect(contact.contains(<ContactDetails />)).toBe(true)
  })

  it('has empty Contact-Name by default', () => {
    expect(contact.contains(<ContactName />)).toBe(true)
  })

  it('contains Contact-Photo, which is by default blank', () => {
    expect(contact.contains(<ContactPhoto />)).toBe(true)
  })
})

describe('Contact with one more class', () => {
  const contact = shallow(<Contact className='One-More' />)

  it('has class Contact plus that additional class', () => {
    expect(contact.prop('className')).toBe('Contact One-More')
  })
})

describe('Contact with photo', () => {
  const contact = shallow(<Contact photo='smiley.png' />)

  it('passes that photo to Contact-Photo as src', () => {
    expect(contact.contains(<ContactPhoto src='smiley.png' />)).toBe(true)
  })
})

describe('Contact with photo2x', () => {
  const contact = shallow(<Contact photo2x='smiley@2x.png' />)

  it('adds "2x" suffix and passes that to Contact-Photo as srcSet', () => {
    expect(contact.contains(<ContactPhoto srcSet='smiley@2x.png 2x' />))
      .toBe(true)
  })
})

describe('Contact with name', () => {
  const contact = shallow(<Contact name='Placeholder Name' />)

  it('now contains Contact-Name with that name', () => {
    expect(contact.contains(<ContactName>Placeholder Name</ContactName>))
      .toBe(true)
  })
})

describe('Contact with position', () => {
  const contact = shallow(<Contact position='Treasurer' />)

  it('passes the position as a child to Contact-About', () => {
    expect(contact.contains(<ContactAbout>Treasurer</ContactAbout>))
      .toBe(true)
  })
})

describe('Contact with location', () => {
  const contact = shallow(<Contact location='Arkhangelsk' />)

  it('passes that location to Contact-About in children prop', () => {
    expect(contact.contains(<ContactAbout>Arkhangelsk</ContactAbout>))
      .toBe(true)
  })
})

describe('Contact with telegram', () => {
  const contact = shallow(<Contact telegram='telegram' />)

  it('passes that to Contact-Telegram', () => {
    expect(contact.contains(<ContactTelegram>telegram</ContactTelegram>))
      .toBe(true)
  })
})

describe('Contact with email', () => {
  const contact = shallow(<Contact email='email@domain.example' />)

  it('passes that email to Contact-Email', () => {
    expect(contact.contains(<ContactEmail>email@domain.example</ContactEmail>))
      .toBe(true)
  })
})

describe('Contact with phone', () => {
  const contact = shallow(<Contact phone='+7 123 456-78-90' />)

  it('passes that phone to Contact-Phone', () => {
    expect(contact.contains(<ContactPhone>+7 123 456-78-90</ContactPhone>))
      .toBe(true)
  })
})
