import React from 'react'
import { shallow } from 'enzyme'

import { ContactDetails } from './Contact-Details'

describe('Contact-Details', () => {
  const contactDetails = shallow(<ContactDetails />)

  it('does not render anything by default', () => {
    expect(contactDetails.type()).toBe(null)
  })
})

describe('Contact-Details with a falsy child', () => {
  const contactDetails = shallow(<ContactDetails>{false}</ContactDetails>)

  it('still does not render anything', () => {
    expect(contactDetails.type()).toBe(null)
  })
})

describe('Contact-Details with a single child', () => {
  const contactDetails = shallow(
    <ContactDetails>
      <a href='https://example.com' />
    </ContactDetails>
  )

  it('is <address>', () => {
    expect(contactDetails.is('address')).toBe(true)
  })

  it('has class Contact-Details', () => {
    expect(contactDetails.prop('className')).toBe('Contact-Details')
  })

  it('contains one child and one <br />', () => {
    expect(contactDetails.contains(<a href='https://example.com' />)).toBe(true)
    expect(contactDetails.find('br').length).toBe(1)
  })
})

describe('Contact-Details a truthy child and a falsy child', () => {
  const contactDetails = shallow(
    <ContactDetails>
      <a href='https://example.com' />
      {false}
    </ContactDetails>
  )

  it('contains one child and one <br />', () => {
    expect(contactDetails.contains(<a href='https://example.com' />)).toBe(true)
    expect(contactDetails.find('br').length).toBe(1)
  })
})

describe('Contact-Details with two children', () => {
  const contactDetails = shallow(
    <ContactDetails>
      <a href='https://example.com' />
      <a href='mailto:email@address.example' />
    </ContactDetails>
  )

  it('includes these two children and two <br />', () => {
    expect(contactDetails.find('br').length).toBe(2)
    expect(contactDetails.contains(<a href='https://example.com' />)).toBe(true)
    expect(contactDetails.contains(<a href='mailto:email@address.example' />))
      .toBe(true)
  })
})
