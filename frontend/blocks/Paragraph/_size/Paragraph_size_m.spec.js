import React from 'react'
import { shallow } from 'enzyme'

import { Paragraph as BaseParagraph } from '../Paragraph'
import { ParagraphSizeM } from './Paragraph_size_m'

const Paragraph = ParagraphSizeM(BaseParagraph)

describe('Paragraph_size_m', () => {
  const paragraph = shallow(<Paragraph size='m' />)

  it('has classes Paragraph and Paragraph_size_m', () => {
    expect(paragraph.prop('className')).toBe('Paragraph Paragraph_size_m')
  })
})
