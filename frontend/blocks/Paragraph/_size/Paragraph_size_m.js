import { withBemMod } from '@bem-react/core'

import { cnParagraph } from '../Paragraph'
import './Paragraph_size_m.css'

export const ParagraphSizeM = withBemMod(cnParagraph(), { size: 'm' })
