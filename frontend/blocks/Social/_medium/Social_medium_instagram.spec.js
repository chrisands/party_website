import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumInstagram } from './Social_medium_instagram'

const Social = SocialMediumInstagram(BaseSocial)

describe('Social_medium_instagram', () => {
  const socialMediumInstagram = shallow(<Social medium='instagram' />)

  it('has classes Social and Social_medium_instagram', () => {
    expect(socialMediumInstagram.prop('className'))
      .toBe('Social Social_medium_instagram')
  })

  it('contains an icon', () => {
    expect(socialMediumInstagram.prop('icon')).toBeTruthy()
  })
})
