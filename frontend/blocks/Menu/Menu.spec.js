import React from 'react'
import { shallow } from 'enzyme'

import { MenuHeader } from './-Header/Menu-Header'
import { MenuNav } from './-Nav/Menu-Nav'
import { Menu } from './Menu'

describe('Menu', () => {
  const menu = shallow(<Menu />)

  it('is <div>', () => {
    expect(menu.is('div')).toBe(true)
  })

  it('has class Menu by default', () => {
    expect(menu.prop('className')).toBe('Menu')
  })

  it('has Menu-Header without predefined onCloseClick', () => {
    expect(menu.contains(<MenuHeader />)).toBe(true)
  })

  it('by default has Menu-Nav without activeItem', () => {
    expect(menu.contains(<MenuNav />)).toBe(true)
  })
})

describe('Menu with one more class', () => {
  const menu = shallow(<Menu className='OneMore' />)

  it('appends that class to the default one', () => {
    expect(menu.prop('className')).toBe('Menu OneMore')
  })
})

describe('Menu with onClose function', () => {
  const onCloseFunc = () => 'I am a function'
  const menu = shallow(<Menu onClose={onCloseFunc} />)

  it('passes that function to Menu-Header as onCloseClick', () => {
    expect(menu.contains(<MenuHeader onCloseClick={onCloseFunc} />)).toBe(true)
  })
})

describe('Menu with activeItem', () => {
  const menu = shallow(<Menu activeItem='party' />)

  it('passes that to Menu-Nav', () => {
    expect(menu.contains(<MenuNav activeItem='party' />)).toBe(true)
  })
})
