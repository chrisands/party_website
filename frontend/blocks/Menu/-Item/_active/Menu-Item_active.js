import { withBemMod } from '@bem-react/core'

import { cnMenu } from '../../Menu'
import './Menu-Item_active.css'

export const MenuItemActive = withBemMod(cnMenu('Item'), { active: true })
