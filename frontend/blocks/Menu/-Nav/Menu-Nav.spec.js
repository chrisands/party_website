import React from 'react'
import { shallow } from 'enzyme'

import { MenuList } from '../-List/Menu-List'
import { MenuNav } from './Menu-Nav'

describe('Menu-Nav', () => {
  const menuNav = shallow(<MenuNav />)

  it('is <nav>', () => {
    expect(menuNav.is('nav')).toBe(true)
  })

  it('has class Menu-Nav', () => {
    expect(menuNav.prop('className')).toBe('Menu-Nav')
  })

  it('contains Menu-List without activeItem by default', () => {
    expect(menuNav.contains(<MenuList />)).toBe(true)
  })
})

describe('Menu-Nave with activeItem', () => {
  const menuNav = shallow(<MenuNav activeItem='item' />)

  it('passes that activeItem to Menu-List', () => {
    expect(menuNav.contains(<MenuList activeItem='item' />)).toBe(true)
  })
})
