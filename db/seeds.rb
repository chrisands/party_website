# frozen_string_literal: true

Dir[Rails.root.join('db', 'seeds', '*.yml')].each do |seed|
  model = File.basename(seed, '.yml').classify.constantize
  model.delete_all
  model.create(YAML.safe_load(File.read(seed)))
end

# Create a user for development to browse /admin and stuff.
if Rails.env.development?
  user_factory = ValidUserFactory.new(
    admin: true,
    name: 'Dev User',
    email: 'dev@email.example',
    password: 'password'
  )

  user = user_factory.produce
  user.save
end
