# frozen_string_literal: true

class InitialMigration < ActiveRecord::Migration[6.0]
  # rubocop:disable Metrics/AbcSize,Metrics/MethodLength
  def change
    # These are extensions that must be enabled in order to support this
    # database. To CREATE EXTENSION "pgcrypto" your database user must be
    # a superuser.
    enable_extension :pgcrypto
    enable_extension :plpgsql

    create_table :active_storage_attachments, if_not_exists: true do |t|
      t.string :name
      t.string :record_type
      t.uuid :record_id
      t.bigint :blob_id
      t.datetime :created_at
    end

    add_index_if_not_exists :active_storage_attachments, :blob_id
    add_index_if_not_exists :active_storage_attachments,
                            %i[record_type record_id name blob_id],
                            name: :index_active_storage_attachments_uniqueness,
                            unique: true

    create_table :active_storage_blobs, if_not_exists: true do |t|
      t.string :key
      t.string :filename
      t.string :content_type
      t.text :metadata
      t.bigint :byte_size
      t.string :checksum
      t.datetime :created_at
    end

    add_index_if_not_exists :active_storage_blobs, :key, unique: true

    create_table :federal_subjects, id: :uuid, if_not_exists: true do |t|
      t.string :name
      t.datetime :created_at
      t.datetime :updated_at
    end

    create_table :illustrations, id: :uuid, if_not_exists: true do |t|
      t.string :alt
      t.uuid :post_id
      t.datetime :created_at
      t.datetime :updated_at
    end

    add_index_if_not_exists :illustrations, :post_id

    create_table :invites, id: :uuid, if_not_exists: true do |t|
      t.string :comment
      t.datetime :created_at
      t.datetime :updated_at
    end

    create_table :membership_requests, id: :uuid, if_not_exists: true do |t|
      t.string :full_name
      t.date :birthdate
      t.string :phone
      t.uuid :federal_subject_id
      t.string :residence
      t.string :email
      t.string :telegram
      t.string :occupation
      t.text :public_organizations
      t.text :comment
      t.datetime :created_at
      t.datetime :updated_at
      t.string :district
    end

    add_index_if_not_exists :membership_requests, :federal_subject_id

    create_table :pictures, id: :uuid, if_not_exists: true do |t|
      t.string :picturable_type
      t.uuid :picturable_id
      t.datetime :created_at
      t.datetime :updated_at
    end

    add_index_if_not_exists :pictures,
                            %i[picturable_type picturable_id],
                            unique: true

    create_table :posts, id: :uuid, if_not_exists: true do |t|
      t.string :title
      t.jsonb :body
      t.datetime :published_at
      t.datetime :created_at
      t.datetime :updated_at
      t.uuid :user_id
      t.string :annotation
      t.string :type
      t.boolean :published
    end

    add_index_if_not_exists :posts, :user_id

    create_table :regional_branches, id: :uuid, if_not_exists: true do |t|
      t.uuid :federal_subject_id
      t.string :telegram_chat
      t.string :trello_list
      t.datetime :created_at
      t.datetime :updated_at
    end

    add_index_if_not_exists :regional_branches, :federal_subject_id

    create_table :users, id: :uuid, if_not_exists: true do |t|
      t.string :name
      t.string :email
      t.string :encrypted_password
      t.string :salt
      t.string :activation_state
      t.string :activation_token
      t.datetime :activation_token_expires_at
      t.string :remember_me_token
      t.datetime :remember_me_token_expires_at
      t.string :reset_password_token
      t.datetime :reset_password_token_expires_at
      t.datetime :reset_password_email_sent_at
      t.datetime :created_at
      t.datetime :updated_at
      t.uuid :invite_id
      t.boolean :admin
    end

    add_index_if_not_exists :users, :activation_token
    add_index_if_not_exists :users, :email
    add_index_if_not_exists :users, :invite_id
    add_index_if_not_exists :users, :remember_me_token
    add_index_if_not_exists :users, :reset_password_token
  end
  # rubocop:enable Metrics/AbcSize,Metrics/MethodLength
end

def add_index_if_not_exists(table, columns, options = {})
  add_index(table, columns, options) unless index_exists?(table, columns)
end
